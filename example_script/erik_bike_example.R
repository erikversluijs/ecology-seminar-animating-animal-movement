## Animation Erik bike

rm(list=ls())
setwd("/mnt/Data/R/workshop_animation/presentation/example_script")

# --- packages ---

library(moveVis)
library(lubridate)
library(plotKML)
library(dplyr)
library(readr)

# --- Prepare data ---

# Load GPX 

df <-  readGPX("erik_bike.gpx")
df <- as.data.frame(df$tracks)

df <- df %>%
  select(Longitude = X2019.10.10.17.51.26.lon,
         Latitude = X2019.10.10.17.51.26.lat,
         DateTime = X2019.10.10.17.51.26.time) %>%
  mutate(DateTime = ymd_hms(DateTime, tz = ""))

df$Id <- "Erik"


# --- animation ---

# create spatial frames with OSM topographic map--

# Convert a data.frame into a move or moveStack object
df_s <- df2move(df,
                proj = "+proj=longlat +datum=WGS84 +no_def", 
                x = "Longitude", 
                y = "Latitude", 
                time = "DateTime", 
                track_id = "Id")

# Aligns movement data to a uniform time scale
df_m <- align_move(df_s, res = 5, unit = "secs")
view_spatial(df_m)

# Create frames of spatial movement maps for animation
frames <- frames_spatial(df_m,
                         map_service = "osm", 
                         map_type = "topographic",
                         path_colours = "red") %>% 
  add_labels(x = "Longitude", y = "Latitude") %>% 
  add_northarrow() %>% 
  add_scalebar(distance = 1, height = 0.02) %>% 
  add_timestamps(df_m, type = "label")

frames = add_gg(frames, 
                gg = expr(labs(title = "Erik's fantastic bike tour (in the rain)",
                               caption = "Made by Erik Versluijs with Movevis"))
)

frames[100] # check 1 frame


# animate frames and save as GIF

animate_frames(frames, out_file = "erik_bike.gif", 
               fps = 10, overwrite = TRUE, 
               res = 200, width = 1400, height = 1400)