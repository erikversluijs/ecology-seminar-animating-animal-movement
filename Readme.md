# Context

This repository contains the presentation about the package moveVis held in Evenstad on 18th November 2020. 


# Viewing the presentation:

To view the presentation download the following files and folders:

  - **animal_movement_animation.html**: the presentation: download and extract the entire folder to see the pictures and videos in the html. 
  
  - pictures/: pictures used in the presentation
  
  - gif/: gifs and videos used in the presentation
  
  - libs/: to properly view the html presentation
  

# Example script

For the example script and data download the following folder:

  - example_script/: contains a R-script and a GPX-file which the movement data


# More information about moveVis

More information at:
http://www.movevis.org

Find the package at: 
https://cloud.r-project.org/package=moveVis

Problems? Look at their github page:
https://github.com/16eagle/moveVis

Find the paper here:
https://doi.org/10.1111/2041-210X.13374